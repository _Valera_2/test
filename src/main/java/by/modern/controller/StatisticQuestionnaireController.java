package by.modern.controller;

import by.modern.domain.Voting;
import by.modern.service.VotingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatisticQuestionnaireController {
    @Autowired
    private VotingService votingService;

    @GetMapping(value = {"/questionnaire/findByLink/{link}"})
    public Voting findVotingByLinkAndOpenStatus(@PathVariable String link) {
        return votingService.findVotingByLinkAndOpenStatus(link);
    }
}
