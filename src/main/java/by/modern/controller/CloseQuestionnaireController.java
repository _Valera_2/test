package by.modern.controller;

import by.modern.service.VotingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CloseQuestionnaireController {
    @Autowired
    private VotingService votingService;

    @PutMapping(value = {"/questionnaire/close/{idVoting}"})
    public void closeVoting(@PathVariable Long idVoting) {
        votingService.closeVoting(idVoting);
    }
}
