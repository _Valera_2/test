package by.modern.controller;

import by.modern.domain.Voting;
import by.modern.dto.QuestionnaireDto;
import by.modern.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CreateQuestionnaireController {
    @Autowired
    private QuestionnaireService questionnaireService;

    @PostMapping(value = {"/questionnaire/create"})
    @ResponseStatus(HttpStatus.CREATED)
    public Voting createVoting(@RequestBody @Valid QuestionnaireDto questionnaireDto) {
        return questionnaireService.saveQuestionnaire(questionnaireDto);
    }
}
