package by.modern.controller;

import by.modern.dto.CountExecutionDto;
import by.modern.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ExecutionQuestionnaireController {
    @Autowired
    private AnswerService answerService;

    @PutMapping(value = {"/questionnaire/execution"})
    public void incrementCountAnswer(@RequestBody @Valid CountExecutionDto countExecutionDto) {
        answerService.updateCount(countExecutionDto.getIdAnswerList());
    }
}
