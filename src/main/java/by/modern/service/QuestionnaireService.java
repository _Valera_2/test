package by.modern.service;

import by.modern.domain.Voting;
import by.modern.dto.QuestionnaireDto;

public interface QuestionnaireService {
    Voting saveQuestionnaire(QuestionnaireDto questionnaire);
}
