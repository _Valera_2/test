package by.modern.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VotingNotFoundException extends RuntimeException {

    public VotingNotFoundException(String link) {
        super("Could not find open voting by link '" + link + "'.");
    }

    public VotingNotFoundException(Long idVoting) {
        super("Could not find voting by idVoting '" + idVoting + "'.");
    }
}
