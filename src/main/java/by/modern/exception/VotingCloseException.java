package by.modern.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VotingCloseException extends RuntimeException {

    public VotingCloseException(Long idVoting) {
        super("Voting closed by idVoting '" + idVoting + "'.");
    }
}
