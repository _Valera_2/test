package by.modern.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CountExecutionDto {
    @NonNull
    private List<Long> idAnswerList;
}
