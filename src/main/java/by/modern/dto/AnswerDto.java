package by.modern.dto;

import lombok.*;

@Getter
@Setter
public class AnswerDto {
    @NonNull
    private String textAnswer;
}
