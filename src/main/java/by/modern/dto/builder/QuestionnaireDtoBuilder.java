package by.modern.dto.builder;

import by.modern.dto.QuestionDto;
import by.modern.dto.QuestionnaireDto;

import java.util.List;

public class QuestionnaireDtoBuilder {
    private QuestionnaireDto questionnaireDto;

    public QuestionnaireDtoBuilder() {
        questionnaireDto = new QuestionnaireDto();
    }

    public QuestionnaireDtoBuilder theme(String theme) {
        questionnaireDto.setTheme(theme);
        return this;
    }

    public QuestionnaireDtoBuilder questionList(List<QuestionDto> questionList) {
        questionnaireDto.setQuestionList(questionList);
        return this;
    }

    public QuestionnaireDto build() {
        return questionnaireDto;
    }
}
