package by.modern.dto.builder;

import by.modern.dto.AnswerDto;
import by.modern.dto.QuestionDto;

import java.util.List;

public class QuestionDtoBuilder {
    private QuestionDto questionDto;

    public QuestionDtoBuilder() {
        questionDto = new QuestionDto();
    }

    public QuestionDtoBuilder textQuestion(String textQuestion) {
        questionDto.setTextQuestion(textQuestion);
        return this;
    }

    public QuestionDtoBuilder answerList(List<AnswerDto> answerDtoList) {
        questionDto.setAnswerList(answerDtoList);
        return this;
    }

    public QuestionDto build() {
        return questionDto;
    }
}
