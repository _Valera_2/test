package by.modern.dto.builder;

import by.modern.dto.AnswerDto;

public class AnswerDtoBuilder {
    private AnswerDto answerDto;

    public AnswerDtoBuilder() {
        answerDto = new AnswerDto();
    }

    public AnswerDtoBuilder textAnswer(String textAnswer) {
        answerDto.setTextAnswer(textAnswer);
        return this;
    }

    public AnswerDto build() {
        return answerDto;
    }
}
