package by.modern.controller;

import by.modern.exception.VotingNotFoundException;
import by.modern.service.VotingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CloseQuestionnaireControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private VotingService votingService;

    @Test
    public void closeVotingByIdVoting() throws Exception {
        mockMvc.perform(put("/questionnaire/close/{idVoting}", 1L)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        try {
            assertEquals("Exception", votingService.findVotingByLinkAndOpenStatus("Link 1"));
        } catch (VotingNotFoundException e) {
            assertEquals("Could not find open voting by link 'Link 1'.", e.getMessage());
        }
    }

    @Test
    public void closeVotingByIdVotingAndCloseStatus() throws Exception {
        mockMvc.perform(put("/questionnaire/close/{idVoting}", 1L)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


    @Test
    public void closeVotingByNotFoundIdVoting() throws Exception {
        mockMvc.perform(put("/questionnaire/close/{idVoting}", 5L)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}