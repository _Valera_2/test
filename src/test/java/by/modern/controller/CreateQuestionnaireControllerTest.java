package by.modern.controller;

import by.modern.dto.AnswerDto;
import by.modern.dto.QuestionDto;
import by.modern.dto.QuestionnaireDto;
import by.modern.dto.builder.AnswerDtoBuilder;
import by.modern.dto.builder.QuestionDtoBuilder;
import by.modern.dto.builder.QuestionnaireDtoBuilder;
import by.modern.utils.JsonUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CreateQuestionnaireControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createVoting() throws Exception {
        mockMvc.perform(post("/questionnaire/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.convertObjectToJsonBytes(getQuestionnaireDto()))
        )
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.theme", is("theme")))
                .andExpect(jsonPath("$.status.name", is("Open")))
                .andExpect(jsonPath("$.questionList.[0].textQuestion", is("question 1")))
                .andExpect(jsonPath("$.questionList.[0].answerList.[0].textAnswer", is("answer 1")))
                .andExpect(jsonPath("$.questionList.[0].answerList.[0].count", is(0)))
                .andExpect(jsonPath("$.questionList.[0].answerList.[1].textAnswer", is("answer 2")))
                .andExpect(jsonPath("$.questionList.[0].answerList.[1].count", is(0)))
                .andExpect(jsonPath("$.questionList.[1].textQuestion", is("question 2")))
                .andExpect(jsonPath("$.questionList.[1].answerList.[0].textAnswer", is("answer 3")))
                .andExpect(jsonPath("$.questionList.[1].answerList.[0].count", is(0)))
                .andExpect(jsonPath("$.questionList.[1].answerList.[1].textAnswer", is("answer 4")))
                .andExpect(jsonPath("$.questionList.[1].answerList.[1].count", is(0)));
    }

    private QuestionnaireDto getQuestionnaireDto() {
        QuestionnaireDto questionnaireDto = new QuestionnaireDtoBuilder()
                .theme("theme")
                .questionList(getQuestionDtoList())
                .build();
        return questionnaireDto;
    }

    private QuestionDto getQuestionDtoFirst() {
        QuestionDto questionDto = new QuestionDtoBuilder()
                .textQuestion("question 1")
                .answerList(getAnswerDtoListFirst())
                .build();
        return questionDto;
    }

    private QuestionDto getQuestionDtoSecond() {
        QuestionDto questionDto = new QuestionDtoBuilder()
                .textQuestion("question 2")
                .answerList(getAnswerDtoListSecond())
                .build();
        return questionDto;
    }

    private List<QuestionDto> getQuestionDtoList() {
        List<QuestionDto> questionDtoList = new ArrayList<>();
        questionDtoList.add(getQuestionDtoFirst());
        questionDtoList.add(getQuestionDtoSecond());
        return questionDtoList;
    }

    private List<AnswerDto> getAnswerDtoListFirst() {
        List<AnswerDto> answerDtoList = new ArrayList<>();
        answerDtoList.add(getAnswerDtoFirst());
        answerDtoList.add(getAnswerDtoSecond());
        return answerDtoList;
    }

    private List<AnswerDto> getAnswerDtoListSecond() {
        List<AnswerDto> answerDtoList = new ArrayList<>();
        answerDtoList.add(getAnswerDtoThird());
        answerDtoList.add(getAnswerDtoForth());
        return answerDtoList;
    }

    private AnswerDto getAnswerDtoFirst() {
        AnswerDto answerDto = new AnswerDtoBuilder()
                .textAnswer("answer 1")
                .build();
        return answerDto;
    }

    private AnswerDto getAnswerDtoSecond() {
        AnswerDto answerDto = new AnswerDtoBuilder()
                .textAnswer("answer 2")
                .build();
        return answerDto;
    }

    private AnswerDto getAnswerDtoThird() {
        AnswerDto answerDto = new AnswerDtoBuilder()
                .textAnswer("answer 3")
                .build();
        return answerDto;
    }

    private AnswerDto getAnswerDtoForth() {
        AnswerDto answerDto = new AnswerDtoBuilder()
                .textAnswer("answer 4")
                .build();
        return answerDto;
    }
}