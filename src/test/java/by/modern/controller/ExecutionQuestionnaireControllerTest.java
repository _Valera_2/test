package by.modern.controller;

import by.modern.dao.AnswerDao;
import by.modern.dto.CountExecutionDto;
import by.modern.utils.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ExecutionQuestionnaireControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AnswerDao answerDao;

    @Test
    public void incrementCountAnswerByIdAnswerList() throws Exception {
        List<Long> idAnswerList = new ArrayList<>();
        idAnswerList.add(1L);
        idAnswerList.add(2L);
        CountExecutionDto countExecutionDto = new CountExecutionDto();
        countExecutionDto.setIdAnswerList(idAnswerList);
        Map<Long, Long> countOldMap = new HashMap<>();
        for (Long idAnswer : idAnswerList) {
            countOldMap.put(idAnswer, answerDao.findOne(idAnswer).getCount() + 1L);
        }
        mockMvc.perform(put("/questionnaire/execution")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtils.convertObjectToJsonBytes(countExecutionDto))
        )
                .andExpect(status().isOk());
        for (Long idAnswer : idAnswerList) {
            Assert.assertEquals(countOldMap.get(idAnswer), answerDao.findOne(idAnswer).getCount());
        }
    }

    @Test
    public void incrementCountAnswerWithoutIdAnswerList() throws Exception {
        mockMvc.perform(put("/questionnaire/execution")
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());
    }
}