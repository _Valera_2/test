package by.modern.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StatisticQuestionnaireControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void findVotingByLinkAndOpenStatus() throws Exception {
        mockMvc.perform(get("/questionnaire/findByLink/{link}", "Link 3")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void findVotingByWrongLink() throws Exception {
        mockMvc.perform(get("/questionnaire/findByLink/{link}", "Wrong link")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findVotingByLinkAndCloseStatus() throws Exception {
        mockMvc.perform(get("/questionnaire/findByLink/{link}", "Link 2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findVotingWithoutLink() throws Exception {
        mockMvc.perform(get("/questionnaire/findByLink/{link}", "")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}